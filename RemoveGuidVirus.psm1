﻿<#
.Synopsis
   Reverse Hidden folder obfuscation from virus infection
.DESCRIPTION
   Finds all matching links in a folder that have been infected with the GUID/linked virus and reverts to the original folder
.EXAMPLE
    Remove-GuidVirus \\server\folder
.EXAMPLE
    Remove-GuidVirus \\server\folder -DeleteLinks
#>
function Remove-GuidVirus
{
    [CmdletBinding(SupportsShouldProcess=$True)]
    Param
    (
        # Path to search
        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true,
                   Position=0)]
        $path,
        #Delete files instead of rename
        [switch]
        $DeleteLinks,
        [string]
        $extensions = "(exe|pif|scr|bat|cmd)"
    )

    Begin
    {
        $sh = New-Object -COM WScript.Shell
    }
    Process
    {
        if (!(Test-Path "$path\zzzBADLINKS") -and !($DeleteLinks)) { New-Item -Path $path -ItemType directory -Name zzzBADLINKS  }
        $i = 0
        $match_h = @{}
        foreach ($f in Get-ChildItem $path -Filter *.lnk) {
            $targetPath = $sh.CreateShortcut($f.FullName).Arguments
            if ($targetPath -match "RECYCLE.+$extensions.+explorer.+({.+})") {
                $i++
                $match_h.add($f.name,$matches[2])
                Write-Output "Matched $f to $($Matches[2])"
                try { 
                    Rename-Item "$path\$($Matches[2])" "$($f.BaseName)"
                    Get-Item "$path\$($f.BaseName)" -Force | Set-ItemProperty -Name Attributes -Value ([System.IO.FileAttributes]::Normal)
                    if ($DeleteLinks) {
                        del $f.FullName -Force
                    }
                    else {
                        move $f.FullName "$path\zzzBADLINKS"
                    }
                }
                catch {
                    Write-Warning "Failed to clean infected folder $($Matches[2])"
                }
            }
            else {
                Write-Verbose "Skipping link $($f.FullName) that does not match pattern"
            }     
        }
    }
    End
    {
        Write-Host "Cleaned $i Folders"
    }
}