# README #

The RemoveGuidVirus module was created after being infected with a self-propagating virus that was renaming folders to a GUID, hiding them, and creating links to them that opened the virus and folder.

This module looks for paths such as this in the .lnk's to be repaired:
C:\Temp\RECYCLE.BIN\asdasd.pif && explorer {d11fd7e5-4abd-445b-a050-1af7bc3e11e1}

### How do I get set up? ###

* Download this module to C:\Program Files\WindowsPowerShell\Modules

### Usage ###

Rename folders back to original names, and move links to a zzzBadLinks folder:

* Remove-GuidVirus '\\\server\folder'

Rename folders back to original names, and delete links: 

* Remove-GuidVirus '\\\server\folder' -DeleteLinks